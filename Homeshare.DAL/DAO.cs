﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAO
    {
        private const string connectionString = @"Data Source=WAD-02\MSSQLSERVER3;Initial Catalog=HomeShareDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        protected static SqlConnection connection
            = new SqlConnection(connectionString);

        public DAO()
        {

        }
        private SqlConnection GetSqlConnection()
        {
            return connection;
        }

    }
}
