﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAOOptions : DAO, IDAO<Options>
    {
        public Options create(Options obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Options read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Options> readAll()
        {
            List<Options> listOptions = new List<Options>();

            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM Options";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Options options = null;
            foreach (DataRow row in dt.Rows)
            {
                options = new Options(
                (int)row["idOption"],
                row["libelle"].ToString());

                listOptions.Add(options);
            }
            return listOptions;
        }

        public Options update(Options obj)
        {
            throw new NotImplementedException();
        }
    }
}
