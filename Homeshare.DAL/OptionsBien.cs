﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class OptionsBien
    {
        public int IdOption { get; set; }
        public Options Options { get; set; }
        public int IdBien { get; set; }
        public string Valeur { get; set; }

        public OptionsBien(int idOption, Options options, int idBien, string valeur)
        {
            IdOption = idOption;
            Options = options;
            IdBien = idBien;
            Valeur = valeur;
        }

        public OptionsBien()
        {
        }
    }
}
