﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAOMembre : DAO, IDAO<Membre>
    {
        public Membre Connection(string login, string password)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM Membre WHERE login= @login AND password= @password";

            command.CommandText = query;

            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Membre membre = new Membre();

            if (membre != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    membre = new Membre(
                          (int)row["idMembre"],
                          row["nom"].ToString(),
                          row["prenom"].ToString(),
                          row["email"].ToString(),
                          (int)row["pays"],
                          row["telephone"].ToString(),
                          row["login"].ToString(),
                          row["password"].ToString()
                          );
                }
                return membre;
            }
            else
            {
                DAOMembre daoM = new DAOMembre();
                membre = daoM.create(membre);
            }
            return null;
        }


        public Membre create(Membre obj)
        {
            SqlCommand commandCreate = connection.CreateCommand();

            string queryCreate = "INSERT INTO Membre (nom, prenom, email, pays, telephone, login, password) " +
            "OUTPUT inserted.idMembre " +
            "VALUES (@nom, @prenom, @email, @pays, @telephone, @login, @password)";

            commandCreate.CommandText = queryCreate;

            commandCreate.Parameters.AddWithValue("@nom", obj.Nom);
            commandCreate.Parameters.AddWithValue("@prenom", obj.Prenom);
            commandCreate.Parameters.AddWithValue("@email", obj.Email);
            commandCreate.Parameters.AddWithValue("@pays", obj.Pays);
            commandCreate.Parameters.AddWithValue("@telephone", obj.Telephone);
            commandCreate.Parameters.AddWithValue("@login", obj.Login);
            commandCreate.Parameters.AddWithValue("@password", obj.Password);

            connection.Open();
            int idMembre = (int)commandCreate.ExecuteScalar();
            connection.Close();

            return obj;
        }

        public void delete(int id)
        {
            SqlCommand commandDelete = connection.CreateCommand();
            string query = "DELETE FROM Membre WHERE idMembre= " + id;

            commandDelete.CommandText = query;

            connection.Open();
            commandDelete.ExecuteNonQuery();
            connection.Close();
        }

        public Membre read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM Membre WHERE idMembre = " + id;

            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter(command);

            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Membre membre = null;
            foreach (DataRow row in dt.Rows)
            {
                         membre = new Membre(
                         (int)row["idMembre"],
                         row["nom"].ToString(),
                         row["prenom"].ToString(),
                         row["email"].ToString(),
                         (int)row["pays"],
                         row["telephone"].ToString(),
                         row["login"].ToString(),
                         row["password"].ToString()
                         );
            }
            return membre;
        }

        public Membre read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Membre> readAll()
        {
            List<Membre> listMembre = new List<Membre>();
            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM Membre";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Membre membre = null;
            foreach (DataRow row in dt.Rows)
            {
                        membre = new Membre(
                        (int)row["idMembre"],
                        row["nom"].ToString(),
                        row["prenom"].ToString(),
                        row["email"].ToString(),
                        (int)row["pays"],
                        row["telephone"].ToString(),
                        row["login"].ToString(),
                        row["password"].ToString()
                        );

                listMembre.Add(membre);
            }
            return listMembre;
        }

        public Membre update(Membre obj)
        {
            SqlCommand commandUpdate = connection.CreateCommand();

            string query = "UPDATE Membre " +
                "SET Nom=@nom, Prenom=@prenom, Email=@email, Pays=@pays, Telephone=@telephone, Login=@login, Password=@password " +
                "WHERE idMembre = @idMembre";

            commandUpdate.CommandText = query;

            commandUpdate.Parameters.AddWithValue("@nom", obj.Nom);
            commandUpdate.Parameters.AddWithValue("@prenom", obj.Prenom);
            commandUpdate.Parameters.AddWithValue("@email", obj.Email);
            commandUpdate.Parameters.AddWithValue("@pays", obj.Pays);
            commandUpdate.Parameters.AddWithValue("@telephone", obj.Telephone);
            commandUpdate.Parameters.AddWithValue("@login", obj.Login);
            commandUpdate.Parameters.AddWithValue("@password", obj.Password);
            commandUpdate.Parameters.AddWithValue("idMembre", obj.idMembre);

            connection.Open();
            commandUpdate.ExecuteNonQuery();
            connection.Close();

            Membre membre = null;
            return membre;
        }
    }
}
