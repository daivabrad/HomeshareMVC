﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class Membre
    {       
        public int idMembre { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        /// <summary>
        /// ici c'est un id de pays
        /// </summary>
        public int Pays { get; set; }
        public string Telephone { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string PhotoUser { get; set; }
        public bool IsDeleted { get; set; }
        
        //pour inscription
        public Membre(int idMembre, string nom, string prenom, string email, 
        int pays, string telephone, string login, string password)
        {
            this.idMembre = idMembre;
            Nom = nom;
            Prenom = prenom;
            Email = email;
            Pays = pays;
            Telephone = telephone;
            Login = login;
            Password = password;
        }

        public Membre(int idMembre, string login, string password)
        {
            this.idMembre = idMembre;
            Login = login;
            Password = password;
        }

        public Membre()
        {
        }

        
        public Membre(int idMembre, string nom, string prenom, string email)
            : this(idMembre, nom, prenom)
        {
            Email = email;
        }
    }
}
