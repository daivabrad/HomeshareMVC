﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAOOptionsBien : DAO, IDAO<OptionsBien>
    {
        public OptionsBien create(OptionsBien obj)
        {
            SqlCommand commandCreateOptionsBien = connection.CreateCommand();

            string queryOptions = "INSERT INTO OptionsBien (fk_idOption, fk_idBien, valeur) " +
                "OUTPUT inserted.idOption " +
                "VALUES (@idOption, @idBien, @valeur)";

            commandCreateOptionsBien.Parameters.AddWithValue("@idOption", obj.IdOption);
            commandCreateOptionsBien.Parameters.AddWithValue("@idBien", obj.IdBien);
            commandCreateOptionsBien.Parameters.AddWithValue("@valeur", obj.Valeur);

            commandCreateOptionsBien.CommandText = queryOptions;

            connection.Open();
            int idOption = (int)commandCreateOptionsBien.ExecuteScalar();
            connection.Close();

            obj.IdOption = idOption;
            return obj;
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public OptionsBien read(object id)
        {
            throw new NotImplementedException();
        }

        public List<OptionsBien> readAll()
        {
            List<OptionsBien> listOptionsBien = new List<OptionsBien>();
            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM OptionBien";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOOptions daoO = new DAOOptions();

            OptionsBien opbien = null;
            foreach (DataRow row in dt.Rows)
            {
                Options option;               
                option = daoO.read((int)row["fk_idOption"]);                
                opbien = new OptionsBien(
                (int)row["idOption"],
                option,
                (int)row["idBien"],
                row["valeur"].ToString()
                );

                listOptionsBien.Add(opbien);
            }
            return listOptionsBien;
        }

        public OptionsBien update(OptionsBien obj)
        {
            throw new NotImplementedException();
        }
    }
}
