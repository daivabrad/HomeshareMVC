﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class Pays
    {
     

        public int IdPays { get; set; }
        public string Libelle { get; set; }


        public Pays(int idPays, string libelle)
        {
            IdPays = idPays;
            Libelle = libelle;
        }

        public Pays()
        {
        }
    }
}
