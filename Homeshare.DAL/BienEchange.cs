﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class BienEchange
    {
        public int IdBien { get; set; }
        public string Titre { get; set; }
        public string DescCourte { get; set; }
        public string DescLong { get; set; }
        public int NombrePerson{ get; set; }
        public int IdPays { get; set; }
        public Pays Pays { get; set; }
        public string Ville { get; set; }
        public string Rue { get; set; }
        public string Numero { get; set; }
        public string CodePostal { get; set; }
        public string Photo { get; set; }
        public bool AssuranceObligatoire { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime DisabledDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int IdMembre { get; set; }
        public Membre Membre { get; set; }
        public DateTime DateCreation { get; set; }

     public BienEchange(int idBien, string titre, string descCourte, string descLong,
            int nombrePerson, int idPays, Pays pays, string ville, string rue,
            string numero, string codePostal, string photo, bool assuranceObligatoire,
            bool isEnabled, string latitude, string longitude,
            int idMembre, Membre membre, DateTime dateCreation)
        {
            IdBien = idBien;
            Titre = titre;
            DescCourte = descCourte;
            DescLong = descLong;
            NombrePerson = nombrePerson;
            IdPays = idPays;
            Pays = pays;
            Ville = ville;
            Rue = rue;
            Numero = numero;
            CodePostal = codePostal;
            Photo = photo;
            AssuranceObligatoire = assuranceObligatoire;
            IsEnabled = isEnabled;
            Latitude = latitude;
            Longitude = longitude;
            IdMembre = idMembre;
            Membre = membre;
            DateCreation = dateCreation;
        }

        /// <summary>
        /// pour affichage
        /// </summary>
        public BienEchange(int idBien, string titre, string descCourte, string descLong,
            int nombrePerson, int idPays, Pays pays, string ville, string photo)
        {
            IdBien = idBien;
            Titre = titre;
            DescCourte = descCourte;
            DescLong = descLong;
            NombrePerson = nombrePerson;
            IdPays = idPays;
            Pays = pays;
            Ville = ville;
            Photo = photo;
        }

        public BienEchange()
        {
        }

        /// <summary>
        /// pour formulaire
        /// </summary>
        public BienEchange(int idBien, string titre, string descCourte, string descLong,
            int nombrePerson, int idPays, string ville, string rue, string numero,
            string codePostal, string photo, bool assuranceObligatoire, bool isEnabled,
            int idMembre, DateTime dateCreation)
        {
            IdBien = idBien;
            Titre = titre;
            DescCourte = descCourte;
            DescLong = descLong;
            NombrePerson = nombrePerson;
            IdPays = idPays;
            Ville = ville;
            Rue = rue;
            Numero = numero;
            CodePostal = codePostal;
            Photo = photo;
            AssuranceObligatoire = assuranceObligatoire;
            IsEnabled = isEnabled;
            IdMembre = Membre.idMembre;
            DateCreation = dateCreation;
        }

      


    }
}
