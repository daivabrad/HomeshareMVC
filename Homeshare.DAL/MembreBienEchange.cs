﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class MembreBienEchange
    {
        public int IdMembre { get; set; }
        public Membre Membre { get; set; }
        public int IdBien { get; set; }
        public BienEchange BienEchange { get; set; }
        public DateTime DateDebEchange { get; set; }
        public DateTime DateFinEchange { get; set; }
        public bool Assurance { get; set; }
        public bool Valide { get; set; }


        public MembreBienEchange(int idMembre, Membre membre,
            int idBien, BienEchange bienEchange, DateTime dateDebEchange,
            DateTime dateFinEchange, bool assurance, bool valide)
        {
            IdMembre = idMembre;
            Membre = membre;
            IdBien = idBien;
            BienEchange = bienEchange;
            DateDebEchange = dateDebEchange;
            DateFinEchange = dateFinEchange;
            Assurance = assurance;
            Valide = valide;
        }

        public MembreBienEchange()
        {
        }
    }
}
