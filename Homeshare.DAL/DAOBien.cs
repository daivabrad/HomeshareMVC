﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAOBien : DAO, IDAO<BienEchange>
    {
        public BienEchange create(BienEchange obj)
        { 
            int idPays = obj.IdPays;
            connection.Close();

            Membre membre = obj.Membre;
            Pays pays = obj.Pays;
            obj.DateCreation= DateTime.Now;

            SqlCommand command = connection.CreateCommand();

            string query = "INSERT INTO BienEchange (titre, descCourte, descLong, nombrePerson, " +
                "ville, rue, numero, codePostal, photo, assuranceObligatoire, isEnabled, Pays, idMembre, dateCreation) " +
                "OUTPUT inserted.idBien " +
                "VALUES (@titre, @descCourte, @descLong, @nombrePerson, @ville, @rue, @numero, @codePostal, " +
                "@photo, @assuranceObligatoire, @isEnabled, @idPays, @idMembre, @dateCreation)";

            command.Parameters.AddWithValue("@titre", obj.Titre);
            command.Parameters.AddWithValue("@descCourte", obj.DescCourte);
            command.Parameters.AddWithValue("@descLong", obj.DescLong);
            command.Parameters.AddWithValue("@nombrePerson", obj.NombrePerson);
            command.Parameters.AddWithValue("@ville", obj.Ville);
            command.Parameters.AddWithValue("@rue", obj.Rue);
            command.Parameters.AddWithValue("@numero", obj.Numero);
            command.Parameters.AddWithValue("@codePostal", obj.CodePostal);
            command.Parameters.AddWithValue("@photo", obj.Photo);
            command.Parameters.AddWithValue("@assuranceObligatoire", obj.AssuranceObligatoire);
            command.Parameters.AddWithValue("@isEnabled", obj.IsEnabled);
            command.Parameters.AddWithValue("@idPays", obj.IdPays);
            command.Parameters.AddWithValue("@idMembre", obj.Membre.idMembre);
            command.Parameters.AddWithValue("@dateCreation", obj.DateCreation);
            command.CommandText = query;

            connection.Open();
            int id = (int)command.ExecuteScalar();
            connection.Close();

            obj.IdBien = id;
            return obj;
        }

        public BienEchange mapperBien(SqlDataReader lecteurData)
        {
            DAOPays daoP = new DAOPays();
            DAOMembre daoM = new DAOMembre();
            Pays pays;
            Membre membre;
            pays = daoP.read((int)lecteurData["Pays"]);
            membre = daoM.read((int)lecteurData["idMembre"]);

            BienEchange bien = new BienEchange(
            (int)lecteurData["idBien"],
            lecteurData["titre"].ToString(),
            lecteurData["descCourte"].ToString(),
            lecteurData["descLong"].ToString(),
            (int)lecteurData["nombrePerson"],
            (int)lecteurData["idPays"],
            pays,
            lecteurData["ville"].ToString(),
            lecteurData["photo"].ToString());

            return bien;
        }

        public List<BienEchange> getBiensMembre(int idMembre)
        {    
            List<BienEchange> listeBiens = new List<BienEchange>();
            if (Connect())
            {
                SqlCommand command = connection.CreateCommand();
                string query = "sp_RecupBienMembre";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@idMembre", idMembre);
                command.CommandText = query;
                SqlDataReader lecteurData;
                try
                {
                    lecteurData = command.ExecuteReader();
                    while (lecteurData.Read())
                    {
                        BienEchange bien = mapperBien(lecteurData);
                        listeBiens.Add(bien);
                    }
                    lecteurData.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    return null;
                }
                return listeBiens;
            }
            return null;
        }
      
       
        private bool Connect()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<BienEchange> readAll()
        {
            List<BienEchange> listeBiens = new List<BienEchange>();

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idBien, titre, descCourte, descLong, nombrePerson, " +
                "Pays, pays, ville, photo FROM BienEchange";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
           
            DAOPays daoP = new DAOPays();
            foreach (DataRow row in dt.Rows)
            {
                Pays pays;
                pays = daoP.read((int)row["Pays"]);

                BienEchange bien = new BienEchange(
                (int)row["idBien"],
                row["titre"].ToString(),
                row["descCourte"].ToString(),
                row["descLong"].ToString(),
                (int)row["nombrePerson"],
                (int)row["Pays"],
                pays,
                row["ville"].ToString(),
                row["photo"].ToString());

                listeBiens.Add(bien);
            }

            return listeBiens;
        }

        public BienEchange update(BienEchange obj)
        {
            throw new NotImplementedException();
        }

        public BienEchange read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM BienEchange WHERE idBien = " + id;

            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter(command);

            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            
            DAOMembre daoM = new DAOMembre();
            DAOPays daoP = new DAOPays();

            BienEchange bien = null;
            foreach (DataRow row in dt.Rows)
            {
                Pays pays;
                Membre membre;
                pays = daoP.read((int)row["Pays"]);
                membre = daoM.read((int)row["idMembre"]);

                bien = new BienEchange(
                (int)row["idBien"],
                row["titre"].ToString(),
                row["descCourte"].ToString(),
                row["descLong"].ToString(),
                (int)row["nombrePerson"],
                (int)row["Pays"],
                pays,
                row["ville"].ToString(),
                row["rue"].ToString(),
                row["numero"].ToString(),
                row["codePostal"].ToString(),
                row["photo"].ToString(),
                (bool)row["assuranceObligatoire"],
                (bool)row["isEnabled"],
                row["latitude"].ToString(),
                row["longitude"].ToString(),
                (int)row["idMembre"],
                membre,
                (DateTime)row["DateCreation"]);              
            }
          return bien;
        }

        public BienEchange read(object id)
        {
            throw new NotImplementedException();
        }

        public List<BienEchange> readAllByMembre(int idMembre)
        {
            List<BienEchange> listeBiens = new List<BienEchange>();

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM BienEchange " +
            "WHERE idMembre=" + idMembre;
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOMembre daoM = new DAOMembre();
            DAOPays daoP = new DAOPays();

            foreach (DataRow row in dt.Rows)
            {
                Pays pays;
                Membre membre;
                pays = daoP.read((int)row["Pays"]);
                membre = daoM.read((int)row["idMembre"]);

                BienEchange bien = new BienEchange(
                (int)row["idBien"],
                row["titre"].ToString(),
                row["descCourte"].ToString(),
                row["descLong"].ToString(),
                (int)row["nombrePerson"],
                (int)row["Pays"],
                pays,
                row["ville"].ToString(),
                row["rue"].ToString(),
                row["numero"].ToString(),
                row["codePostal"].ToString(),
                row["photo"].ToString(),
                (bool)row["assuranceObligatoire"],
                (bool)row["isEnabled"],
                row["latitude"].ToString(),
                row["longitude"].ToString(),
                (int)row["idMembre"],
                membre,
                (DateTime)row["DateCreation"]); 
          
                listeBiens.Add(bien);
            }

            return listeBiens;
        }
    }
}
