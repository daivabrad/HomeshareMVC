﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class AvisMembreBien
    {
        public int IdAvis { get; set; }
        [DisplayName("Ranking")]
        public int Note { get; set; }
        public string Message { get; set; }
        public int IdMembre { get; set; }
        public Membre Membre { get; set; }
        public int IdBien { get; set; }
        public BienEchange BienEchange { get; set; }
        public DateTime DateAvis { get; set; }
        public bool Approuve { get; set; }

        /// <summary>
        /// pour creer un commentaire (formulaire)
        /// </summary>
     
        public AvisMembreBien(int idAvis, int note, string message, int idMembre,
            Membre membre, int idBien, BienEchange bienEchange, DateTime dateAvis)
        {
            IdAvis = idAvis;
            Note = note;
            Message = message;
            IdMembre = idMembre;
            Membre = membre;
            IdBien = idBien;
            BienEchange = bienEchange;
            DateAvis = dateAvis;
        }

        /// <summary>
        /// pour approuvé 
        /// </summary>
        public AvisMembreBien(int idAvis, int note, string message, int idMembre,
            Membre membre, int idBien, BienEchange bienEchange, DateTime dateAvis,
            bool approuve): this(
                idAvis, note, message, idMembre, membre, idBien, bienEchange, dateAvis
                )
        {
            Approuve = approuve;
        }

        public AvisMembreBien()
        {
        }
    }
}
