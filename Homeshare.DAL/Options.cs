﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class Options
    {
        public int IdOption { get; set; }
        public string Libelle { get; set; }


        public Options(int idOption, string libelle)
        {
            IdOption = idOption;
            Libelle = libelle;
        }

        public Options()
        {
        }
    }
}
