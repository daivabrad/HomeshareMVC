﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class DAOPays : DAO, IDAO<Pays>
    {
        public Pays create(Pays obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Pays read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idPays, libelle FROM Pays WHERE idPays=" + id;
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Pays pays = null;
            foreach (DataRow row in dt.Rows)
            {
                pays = new Pays((int)row["idPays"],
                row["libelle"].ToString()
                );
            }
            return pays;
        }

        public Pays read(object id)
        {
            throw new NotImplementedException();
        }

        public List<Pays> readAll()
        {
            List<Pays> listPays = new List<Pays>();
            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM Pays";
            command.CommandText = query;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Pays pays = null;
            foreach (DataRow row in dt.Rows)
            {
                pays = new Pays(
                (int)row["idPays"],
                row["libelle"].ToString()
                );

                listPays.Add(pays);
            }
            return listPays;
        }

        public Pays update(Pays obj)
        {
            throw new NotImplementedException();
        }
    }
}
