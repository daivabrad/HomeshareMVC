﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homeshare.DAL
{
    public class HistoAvis
    {
        public int IdAvis { get; set; }
        public DateTime DateAvis { get; set; }
        public int NoteAvis { get; set; }
        public int IdMembre { get; set; }
        public Membre Membre { get; set; }
        public int IdProprio { get; set; }
        public string TitreBien { get; set; }
        public string VilleBien { get; set; }
        public int NbrEchange { get; set; }


        public HistoAvis(int idAvis, DateTime dateAvis, int noteAvis, int idMembre,
            Membre membre, int idProprio, string titreBien, string villeBien, int nbrEchange)
        {
            IdAvis = idAvis;
            DateAvis = dateAvis;
            NoteAvis = noteAvis;
            IdMembre = idMembre;
            Membre = membre;
            IdProprio = idProprio;
            TitreBien = titreBien;
            VilleBien = villeBien;
            NbrEchange = nbrEchange;
        }

        public HistoAvis()
        {
        }
    }
}
