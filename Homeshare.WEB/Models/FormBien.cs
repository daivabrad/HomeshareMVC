﻿using Homeshare.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Homeshare.WEB.Models
{
    public class FormBien
    {
        public BienEchange BienEchange { get; set; }
        public Membre Membre { get; set; }
        public List<Options> Options { get; set; }
        public List<Pays> ListPays { get; set; }


        public FormBien(BienEchange bienEchange, Membre membre, List<Options> options, List<Pays> listPays)
        {
            BienEchange = bienEchange;
            Membre = membre;
            Options = options;
            ListPays = listPays;
        }

        public FormBien()
        {
        }
    }
}