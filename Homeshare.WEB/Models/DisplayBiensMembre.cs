﻿using Homeshare.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Homeshare.WEB.Models
{
    public class DisplayBiensMembre
    {
        public Membre  Membre { get; set; }
        public List<BienEchange> ListeBiens { get; set; }

        public DisplayBiensMembre(Membre membre, List<BienEchange> listeBiens)
        {
            Membre = membre;
            ListeBiens = listeBiens;
        }

        public DisplayBiensMembre()
        {
        }
    }
}