﻿using Homeshare.DAL;
using Homeshare.WEB.Infra;
using Homeshare.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homeshare.WEB.Controllers
{
    public class MembreController : Controller
    {
        // GET: Membre
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult MembrePage()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Connection()
        {
            return View();
        }

        private DAOMembre daoM = new DAOMembre();
        private DAOPays daoP = new DAOPays();
        private DAOBien daoB = new DAOBien();
        private DAOOptions daoO = new DAOOptions();
        private DAOOptionsBien daoOB = new DAOOptionsBien();

        [HttpPost]
        public ActionResult Connection(string login, string password)
        {
           Membre membre = daoM.Connection(login, password);
           SessionManager.UserConnected = membre;
          
            if (membre.Email == null || membre.Password == null)
            {
                return RedirectToAction("CreateAccount", "Membre");
            }
            else
            {
                Session["UserConnected"] = membre;
                return RedirectToAction("MembrePage", "Membre");
            }

        }

        [HttpGet]
        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAccount(Membre membre)
        {
            if (membre.Prenom == null || membre.Email == null || membre.Password == null)
            {
                ViewBag.Message = "Error! Il faut remplir les champs.";
                return View(membre);
            }
            daoM.create(membre);

            return RedirectToAction("Connection", "Membre");
        }

        public ActionResult DeleteMembre(int id)
        {
            Membre membre = daoM.read(id);
            daoM.delete(id);
            return RedirectToAction("ListMembres", "Membre");
        }

        [HttpGet]
        public ActionResult AjouterBien(int id)
        {
            List<Options> options = daoO.readAll();
            List<Pays> listPays = daoP.readAll();
            Membre membre = daoM.read(id);
            FormBien formBien = new Models.FormBien(null, membre, options, listPays);
            return View(formBien);
        }

        [HttpPost]
        public ActionResult AjouterBien(FormBien formBien)
        {
            BienEchange bien = daoB.create(formBien.BienEchange);

            return RedirectToAction("Index", "Home");
            //return RedirectToAction("ListBien", "Membre");
        }


        [HttpGet]
        public ActionResult DetailsMembre(int id)
        {
            Membre membre = daoM.read(id);
            return View(membre);
        }

        [HttpGet]
        public ActionResult ListMembres()
        {
            List<Membre> membres = daoM.readAll();
            return View(membres);
        }

        [HttpGet]
        public ActionResult UpdateMembre(int id)
        {
            Membre membre= daoM.read(id);
            return View(membre);
        }

        [HttpPost]
        public ActionResult UpdateMembre(Membre membre)
        {
            daoM.update(membre);
            return RedirectToAction("ListMembres", "Membre");
        }

        /// <summary>
        /// ceci ne marche pas
        /// </summary>
        
        [HttpGet]
        public ActionResult GetBiensMembre(int id)
        {
            Membre membre = daoM.read(id);
            List<BienEchange> listeBiens = daoB.getBiensMembre(id);
            DisplayBiensMembre biensMembre = new DisplayBiensMembre(membre, listeBiens);

            return View(biensMembre);
        }


        [HttpGet]
        public ActionResult ListBiens()
        {
            List<BienEchange> biens = daoB.readAll();
            return View(biens);
        }

        [HttpGet]
        public ActionResult DetailsBien(int id)
        {
            BienEchange bien = daoB.read(id);
            return View(bien);
        }

        [HttpGet]
        public ActionResult BiensParMembre(int id)
        {
            Membre membre = daoM.read(id);
            List<BienEchange> biens = daoB.readAllByMembre(id);
            DisplayBiensMembre form = new Models.DisplayBiensMembre(membre, biens);

            return View(form);
        }
    }
}