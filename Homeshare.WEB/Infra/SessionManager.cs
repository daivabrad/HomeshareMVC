﻿using Homeshare.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Homeshare.WEB.Infra
{
    public class SessionManager
    {
        public static Membre UserConnected
        {
            get
            {
                if (HttpContext.Current.Session["UserConnected"] == null)
                    HttpContext.Current.Session["UserConnected"] = default(Membre);
                return (Membre)HttpContext.Current.Session["UserConnected"];
            }

            set { HttpContext.Current.Session["UserConnected"] = value; }
        }

        public static int? idUser
        {
            get { return (int?)HttpContext.Current.Session["idUser"]; }
            set { HttpContext.Current.Session["idUser"] = value; }
        }

    }
}